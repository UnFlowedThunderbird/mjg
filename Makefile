TARGET=unflowed

XPI_FILES=chrome.manifest chrome/$(TARGET).jar install.rdf readme.txt
JAR_FILES=$(wildcard content/* locale/*/* skin/*)

ID=$(shell sed -n -e 's/^.*<em:id>\(.*\)<\/em:id>.*/\1/p' -e '/em:name/q' \
	< install.rdf)
PROFILE_DIRECTORY=$(wildcard $(HOME)/.thunderbird/*.default)

$(TARGET).xpi: $(XPI_FILES)
	zip -9r $@ $^

chrome/$(TARGET).jar: $(JAR_FILES)
	mkdir -p chrome
	zip -9r $@ $^

symlink:
	ln -s "$(shell pwd)" "$(PROFILE_DIRECTORY)/extensions/$(ID)"
